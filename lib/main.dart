import 'package:flutter/material.dart';
import 'package:pomodoro_todo/pages/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo-Pomodoro',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: HomePage(title: 'PomodoroTodo'),
    );
  }
}


