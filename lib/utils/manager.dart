import 'package:pomodoro_todo/models/task.dart';
import 'package:pomodoro_todo/utils/database.dart';

class Manager {
  Future<List<Task>> tasksData;

  addNewTask(Task task) async {
    await DatabaseUtil.db.insert(task);
  }

  updateTask(Task task) async {
    await DatabaseUtil.db.update(task);
  }

  removeTask(Task task) async {
    await DatabaseUtil.db.remove(task);
  }

  loadAllTasks() {
    tasksData = DatabaseUtil.db.getAll();
  }
}
